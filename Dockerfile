FROM debian:latest

#Stolen from William Yeh <william.pjyeh@gmail.com>

RUN echo "===> Installing python, sudo, and supporting tools..."     && \
    apt-get update && apt-get install -y apt-utils && apt-get upgrade -y && \
    apt-get install -y                              \
      python sudo openssh-client apt-utils git     \
      curl gcc python3-pip python3-dev libffi-dev   \
      sshpass libssl-dev python3-yaml python3-ncclient 

RUN echo " Installing needed python libs " && \
    pip3 install --upgrade setuptools            && \
    pip3 install --upgrade pycrypto              && \
    pip3 install --upgrade ipaddr                && \
    pip3 install --upgrade requests              && \
    pip3 install --upgrade cffi pywinrm 

RUN echo "===> Installing Ansible..."        && \
    pip3 install ncclient jmespath dnspython  &&  \
    pip3 install ansible                  

RUN echo "===>  CLEANUP "                  && \
    apt-get -f -y --auto-remove remove \
      gcc libffi-dev libssl-dev  && \
    apt-get clean                                                 && \
    rm -rf /var/lib/apt/lists/*  /tmp/*                           && \
    \
    echo "===> Adding hosts for convenience..."        && \
    mkdir -p /etc/ansible                              && \
    echo 'localhost' > /etc/ansible/hosts 


# default command: display Ansible version
CMD [ "ansible-playbook", "--version" ]
